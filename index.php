<?php

// Задача 13 (на циклы):
//нужно вывести в столбец числа от 1 до 75;
//нужно использовать 2 разных цикла.

for ($a = 1; $a <= 75; $a++) {
    echo $a . PHP_EOL."<br>";
}


$a = 1;
while ($a < 76) {
    echo $a . PHP_EOL."<br>";
    $a++;
}

//Задача 14 (на циклы):
//есть массив с элементами 7, 5, 3, 1;
//выведите квадрат каждого числа с новой строки.

$arr = [7, 5, 3, 1];
foreach ($arr as $value) {
    $result = $value * $value;
    echo $result . PHP_EOL."<br>";
}

// Задача 8:
// есть переменная $language. В ней может быть 2 варианта значений: 'en_GB' и ‘ru_RU‘';
// нужно написать код, который в случае, если $language имеет значение 'en_GB' -
// в переменную $months запишет массив из 12 месяцев года на английском языке, если language === ‘ru_RU’ – то на русском;
// нужно выполнить задание с помощью 3 способов: через 2 if, через switch-case, через многомерный массив и без if’ов
// и switch-case.

// --a--
$language = "en_GB";
if ($language == "en_GB") {
    $month = ["jan", "feb", "Mar", "Apr", "May", "Jun", "jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
} else {
    $month = ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"];
}

// --b--

switch ($language) {
    case 'en_GB':
        $month = ["jan", "feb", "Mar", "Apr", "May", "Jun", "jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        break;
    case 'ru_RU':
        $month = ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"];
        break;


}

// --c--

$language = ['en_GB' => ["jan", "feb", "Mar", "Apr", "May", "Jun", "jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    'ru_RU' => ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"]];

// Задача 15 (на циклы):
// есть массив $responseStatuses = [
//   100 => 'Сообщения успешно приняты к отправке',
//   105 => 'Ошибка в формате запроса',
//   110 => 'Неверная авторизация',
//   115 => 'Недопустимый IP-адрес отправителя',
// ];
// с помощью цикла выведите на экран столбец строк формата:
// ‘Код ошибки  - 100, текст ошибки - Сообщения успешно приняты к отправке’ (всего 4 строки).

$responseStatuses = [
    100 => 'Сообщения успешно приняты к отправке',
    105 => 'Ошибка в формате запроса',
    110 => 'Неверная авторизация',
    115 => 'Недопустимый IP-адрес отправителя',
];
foreach ($responseStatuses as $key => $value) {
    echo 'Код ошибки - ' . $key . ', текст ошибки - '.$value."<br>";
}
